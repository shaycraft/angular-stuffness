// Angular
import { Injectable } from '@angular/core';

// rxjs
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { Book } from '../models/book.model';
import { REST_ENDPOINT_COLLECTIONS } from '../../core/models/rest-endpoint-collections.enum';
import { RestService } from '../../core/providers/rest.service';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  constructor(private readonly _service: RestService) {}

  // public methods

  /**
   *
   * @returns all books
   */
  public get(): Observable<Book[]> {
    return this._service.getAll<Book>(REST_ENDPOINT_COLLECTIONS.BOOKS);
  }

  /**
   *
   * @param searchString
   * @returns list of books matching search string
   */
  public search(searchString: string): Observable<Book[]> {
    return this._service
      .getAll<Book>(REST_ENDPOINT_COLLECTIONS.BOOKS)
      .pipe(
        map((books): Book[] =>
          books.filter(
            (book: Book) =>
              book.title.indexOf(searchString) > -1 ||
              book.author.indexOf(searchString) > -1
          )
        )
      );
  }

  /**
   *
   * @param id book id to search
   * @returns observable of book that was found
   */
  public searchById(id: number): Observable<Book> {
    return this._service.getSingle<Book>(REST_ENDPOINT_COLLECTIONS.BOOKS, id);
  }

  /**
   *
   * @param isbns
   * @returns list of books with isbn number's added to data
   */
  public getAppraisals(isbns: string[]): Observable<Record<string, number>> {
    const list: Record<string, number> = {};
    isbns.forEach((isbn: string) => {
      list[isbn] = +(Math.random() * 100).toFixed(2);
    });

    return of(list);
  }

  /**
   *
   * @param book
   * @returns updated book object
   */
  public update(book: Book): Observable<Book> {
    return this._service.update<Book>(REST_ENDPOINT_COLLECTIONS.BOOKS, book);
  }

  /**
   *
   * @param book
   * @returns created book object
   */
  public create(book: Book): Observable<Book> {
    return this._service.create<Book>(REST_ENDPOINT_COLLECTIONS.BOOKS, book);
  }
}
