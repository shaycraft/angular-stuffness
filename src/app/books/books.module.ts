// Angular
import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

// toastr
import { ToastrModule } from 'ngx-toastr';

import { BookListContainerComponent } from './containers/book-list-container.component';
import { BooksRoutingModule } from './books-routing.module';
import { BookListComponent } from './components/book-list.component';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { DataService } from '../core/providers/data.service';
import { BookSearchComponent } from './components/book-search.component';
import { BookAddContainerComponent } from './containers/book-add-container.component';
import { BookEditContainerComponent } from './containers/book-edit-container.component';
import { BookUpsertComponent } from './components/book-upsert.component';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    BooksRoutingModule,
    InMemoryWebApiModule.forFeature(DataService),
    ReactiveFormsModule,
    ToastrModule.forRoot(),
  ],
  declarations: [
    BookListContainerComponent,
    BookListComponent,
    BookSearchComponent,
    BookAddContainerComponent,
    BookEditContainerComponent,
    BookUpsertComponent,
  ],
})
export class BooksModule {}
