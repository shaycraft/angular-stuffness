// Angular
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounce, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-book-search',
  templateUrl: './book-search.component.html',
  styleUrls: ['./book-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookSearchComponent implements OnInit {
  // public properties
  public searchText: FormControl = new FormControl('');
  @Output() public searchTextChange: EventEmitter<string> =
    new EventEmitter<string>();

  public ngOnInit(): void {
    this.searchText.valueChanges
      .pipe(debounceTime(150))
      .subscribe((searchText: string) =>
        this.searchTextChange.emit(searchText)
      );
  }

  // events

  /**
   *
   * @param event$ keyboard event
   */
  public onTextChanged($event: KeyboardEvent): void {
    const textArea: HTMLTextAreaElement = $event.target as HTMLTextAreaElement;
    this.searchTextChange.emit(textArea.value);
  }
}
