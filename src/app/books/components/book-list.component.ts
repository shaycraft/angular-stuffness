// Angular
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Book } from '../models/book.model';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookListComponent {
  @Input() public books: Book[] = [];
}
