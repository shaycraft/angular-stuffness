// Angular
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

import { Book } from '../models/book.model';
import { ViewConfig } from '../models/view-config.model';

@Component({
  selector: 'app-book-upsert',
  templateUrl: './book-upsert.component.html',
  styleUrls: ['./book-upsert.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookUpsertComponent {
  // input properties

  @Input()
  public set book(value: Book | null) {
    this._book = value;
    this.patchForm(value);
  }

  @Input() public config?: ViewConfig;

  // output properties

  @Output() public save: EventEmitter<Book> = new EventEmitter<Book>();

  // public properties

  public bookForm: FormGroup;
  public get book(): Book | null {
    return this._book;
  }
  public get isbn() {
    return this.bookForm.get('isbn') as FormControl;
  }
  public get title() {
    return this.bookForm.get('title') as FormControl;
  }
  public get author() {
    return this.bookForm.get('author') as FormControl;
  }
  public get pages() {
    return this.bookForm.get('pages') as FormControl;
  }
  public get yearPublished() {
    return this.bookForm.get('yearPublished') as FormControl;
  }

  // private properties

  private fb: FormBuilder = new FormBuilder();
  private _book: Book | null = null;

  // constructor
  constructor() {
    this.bookForm = this.initializeForm();
  }

  // events

  public onSave(): void {
    this.save.emit(
      new Book({
        ...this.bookForm.value,
        id: this._book?.id,
      })
    );
  }

  // private methods

  /**
   *
   * @returns constructed form group
   */
  private initializeForm(): FormGroup {
    return this.fb.group({
      isbn: new FormControl('', Validators.required),
      title: new FormControl('', Validators.required),
      author: new FormControl('', Validators.required),
      genre: new FormControl(''),
      pages: new FormControl('', Validators.pattern(/^[0-9]*$/)),
      yearPublished: new FormControl('', Validators.pattern(/^[0-9]*$/)),
    });
  }

  /**
   *
   * @param value book object
   */
  private patchForm(value: Book | null): void {
    const patch: { [key: string]: any } = {};
    Object.keys(value || {}).forEach((key: string) => {
      patch[key] = (<Book>value)[key as keyof Book];
    });

    this.bookForm.patchValue(patch);
  }
}
