interface BookInterface {
  id?: number;
  title?: string;
  author?: string;
  genre?: string;
  isbn?: string;
  pages?: number;
  yearPublished?: number;
  appraisal?: number;
}

export class Book implements BookInterface {
  public id: number;
  public title: string;
  public author: string;
  public genre: string;
  public isbn: string;
  public pages: number;
  public yearPublished: number;
  public appraisal: number;

  constructor({
    id,
    title,
    author,
    genre,
    isbn,
    pages,
    yearPublished,
    appraisal,
  }: BookInterface) {
    this.id = id as number;
    this.title = title as string;
    this.author = author as string;
    this.genre = genre as string;
    this.isbn = isbn as string;
    this.pages = pages as number;
    this.yearPublished = yearPublished as number;
    this.appraisal = appraisal as number;
  }
}
