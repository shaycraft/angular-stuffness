interface ViewConfigInterface {
  titleText?: string;
  buttonText?: string;
}

export class ViewConfig implements ViewConfigInterface {
  public titleText?: string;
  public buttonText?: string;

  constructor({ titleText, buttonText }: ViewConfigInterface) {
    this.titleText = titleText;
    this.buttonText = buttonText;
  }
}
