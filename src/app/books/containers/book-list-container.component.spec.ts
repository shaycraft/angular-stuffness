// Angular
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

// rxjs
import { of } from 'rxjs';

import { BookListContainerComponent } from './book-list-container.component';
import { BookListComponent } from '../components/book-list.component';
import { BookSearchComponent } from '../components/book-search.component';

describe('BookContainerComponent', () => {
  let component: BookListContainerComponent;
  let fixture: ComponentFixture<BookListContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      declarations: [
        BookListContainerComponent,
        BookListComponent,
        BookSearchComponent,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    // spies
    spyOn(component['_service'], 'get').and.returnValue(of([]));
    spyOn(component['_service'], 'search').and.returnValue(of([]));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return list', (done: () => void) => {
    component.books$.subscribe((data) => {
      expect(data).toBeTruthy();
      expect(component['_service'].get).toHaveBeenCalled();
      done();
    });
    component.ngOnInit();
  });
});
