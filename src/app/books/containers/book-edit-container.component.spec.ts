// angular
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

// toastr
import { ToastrService } from 'ngx-toastr';

// mocks
import { MockToastrService } from '../../testing/mocks/mock-toastr.service';

import { BookUpsertComponent } from '../components/book-upsert.component';
import { BookService } from '../providers/book.service';
import { BookEditContainerComponent } from './book-edit-container.component';

describe('BookEditContainerComponent', () => {
  let component: BookEditContainerComponent;
  let fixture: ComponentFixture<BookEditContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
      ],
      declarations: [BookEditContainerComponent, BookUpsertComponent],
      providers: [
        BookService,
        {
          provide: ToastrService,
          useClass: MockToastrService,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookEditContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
