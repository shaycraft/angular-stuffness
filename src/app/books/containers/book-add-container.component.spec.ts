// angular
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

// toastr
import { ToastrService } from 'ngx-toastr';

// mocks
import { MockToastrService } from '../../testing/mocks/mock-toastr.service';

import { BookUpsertComponent } from '../components/book-upsert.component';
import { BookService } from '../providers/book.service';
import { BookAddContainerComponent } from './book-add-container.component';

describe('BookAddContainerComponent', () => {
  let component: BookAddContainerComponent;
  let fixture: ComponentFixture<BookAddContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
      ],
      declarations: [BookAddContainerComponent, BookUpsertComponent],
      providers: [
        BookService,
        { provide: ToastrService, useClass: MockToastrService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookAddContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
