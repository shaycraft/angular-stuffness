// angular
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';

// toastr
import { ToastrService } from 'ngx-toastr';

import { Book } from '../models/book.model';
import { BookService } from '../providers/book.service';

@Component({
  selector: 'app-book-add-container',
  templateUrl: './book-add-container.component.html',
  styleUrls: ['./book-add-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class BookAddContainerComponent {
  constructor(
    private _service: BookService,
    private _router: Router,
    private _toastr: ToastrService
  ) {}

  // events

  /**
   *
   * @param $event
   */
  public onSave($event: Book) {
    this._service.create($event).subscribe(
      (res) => {
        this._toastr.success('Book created!');
        this._router.navigate(['books']);
      },
      (err) => {
        this._toastr.error('Error during create!');
        console.error(err);
      }
    );
  }
}
