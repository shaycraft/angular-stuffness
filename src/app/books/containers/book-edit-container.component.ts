// angular
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

// Toastr
import { ToastrService } from 'ngx-toastr';

// rxjs
import { Observable } from 'rxjs';

import { Book } from '../models/book.model';
import { BookService } from '../providers/book.service';

@Component({
  selector: 'app-book-edit-container',
  templateUrl: './book-edit-container.component.html',
  styleUrls: ['./book-edit-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class BookEditContainerComponent implements OnInit {
  // public properties

  public book$: Observable<Book> | undefined = undefined;

  constructor(
    private _route: ActivatedRoute,
    private _service: BookService,
    private readonly _toastr: ToastrService
  ) {}

  public ngOnInit(): void {
    this.book$ = this._service.searchById(this._route.snapshot.params.id);
  }

  // events

  public onSave($event: Book): void {
    this._service.update($event).subscribe(
      (_) => {
        this._toastr.success('Book updated!');
      },
      (err) => {
        this._toastr.error('Error saving book!');
        console.error(err);
      }
    );
  }
}
