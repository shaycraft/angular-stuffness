// Angular
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';

// rxjs
import { Observable, Subject } from 'rxjs';
import { map, switchMap, takeUntil } from 'rxjs/operators';

import { Book } from '../models/book.model';
import { BookService } from '../providers/book.service';

@Component({
  selector: 'app-book-container',
  templateUrl: './book-list-container.component.html',
  styleUrls: ['./book-list-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class BookListContainerComponent implements OnInit, OnDestroy {
  // public properties
  public books$: Subject<Book[]> = new Subject<Book[]>();

  // private properties
  private _unsubscribe$: Subject<void> = new Subject<void>();

  constructor(private readonly _service: BookService) {}

  // angular lifecycle events
  public ngOnInit(): void {
    this._service
      .get()
      .pipe(
        switchMap((books: Book[]) => this.mapAppraisalsToList(books)),
        takeUntil(this._unsubscribe$)
      )
      .subscribe((data: Book[]) => this.books$.next(data));
  }

  public ngOnDestroy(): void {
    if (this._unsubscribe$) {
      this._unsubscribe$.next();
      this._unsubscribe$.complete();
    }
  }

  // events

  /**
   *
   * @param searchText
   */
  public onSearchTextChanged(searchText: string): void {
    this._service
      .search(searchText)
      .pipe(
        switchMap((books: Book[]) => this.mapAppraisalsToList(books)),
        takeUntil(this._unsubscribe$)
      )
      .subscribe((data: Book[]) => this.books$.next(data));
  }

  /// private methods;

  /**
   *
   * @param books
   * @returns mapped books objects with appraisals
   */
  private mapAppraisalsToList(books: Book[]): Observable<Book[]> {
    return this._service
      .getAppraisals(books.map((book: Book) => book.isbn))
      .pipe(
        map((appraisals: Record<string, number>) =>
          books.map((book) => {
            return {
              ...book,
              appraisal: appraisals[book.isbn],
            };
          })
        )
      );
  }
}
