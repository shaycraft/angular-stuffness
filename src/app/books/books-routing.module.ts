// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookAddContainerComponent } from './containers/book-add-container.component';

import { BookListContainerComponent } from './containers/book-list-container.component';
import { BookEditContainerComponent } from './containers/book-edit-container.component';

const routes: Routes = [
  { path: '', component: BookListContainerComponent },
  { path: 'add', component: BookAddContainerComponent },
  { path: 'edit/:id', component: BookEditContainerComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BooksRoutingModule {}
