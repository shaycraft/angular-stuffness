// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OnpushPocComponent } from './components/onpush-poc.component';
import { BindingDemoOverviewComponent } from './components/binding-demo-overview.component';
import { BindingDemoRoutingModule } from './binding-demo-routing.module';
import { DefaultPocComponent } from './components/default-poc.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, BindingDemoRoutingModule],
  declarations: [
    OnpushPocComponent,
    BindingDemoOverviewComponent,
    DefaultPocComponent,
  ],
})
export class BindingDemoModule {}
