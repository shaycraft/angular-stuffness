import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultPocComponent } from './default-poc.component';
import { OnpushPocComponent } from './onpush-poc.component';

describe('DefaultPocComponent', () => {
  let component: DefaultPocComponent;
  let fixture: ComponentFixture<DefaultPocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DefaultPocComponent, OnpushPocComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultPocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
