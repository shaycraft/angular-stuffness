// angular
import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-default-poc',
  templateUrl: './default-poc.component.html',
  styleUrls: ['./default-poc.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class DefaultPocComponent implements AfterViewChecked {
  @Input() public testProperty?: { foo: string };
  public changeCounter: number = 0;

  constructor(private _cdr: ChangeDetectorRef) {}

  public ngAfterViewChecked(): void {
    this.changeCounter++;
    this._cdr.detectChanges();
  }
}
