import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-binding-demo-overview-component',
  templateUrl: './binding-demo-overview.component.html',
  styleUrls: ['./binding-demo-overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class BindingDemoOverviewComponent {
  public objectExample = {
    foo: 'bar',
  };
}
