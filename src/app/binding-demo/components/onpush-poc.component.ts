// angular
import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-onpush-poc',
  templateUrl: './onpush-poc.component.html',
  styleUrls: ['./onpush-poc.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OnpushPocComponent implements AfterViewChecked {
  @Input() public testProperty?: { foo: string };

  public changeCounter: number = 0;

  constructor(public cdr: ChangeDetectorRef) {}

  public ngAfterViewChecked() {
    this.changeCounter++;
  }
}
