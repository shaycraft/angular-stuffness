// angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BindingDemoOverviewComponent } from './components/binding-demo-overview.component';

const routes: Routes = [{ path: '', component: BindingDemoOverviewComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BindingDemoRoutingModule {}
