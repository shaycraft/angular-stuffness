// Angular
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// rxjs
import { Observable } from 'rxjs';

import { REST_ENDPOINT_COLLECTIONS } from '../models/rest-endpoint-collections.enum';
import { RestConfigService } from './rest-config.service';

@Injectable({
  providedIn: 'root',
})
export class RestService {
  constructor(
    private readonly _http: HttpClient,
    private readonly _config: RestConfigService
  ) {}

  /**
   *
   * @param collection
   * @returns list of entity collections
   */
  public getAll<T>(collection: REST_ENDPOINT_COLLECTIONS): Observable<T[]> {
    return this._http.get<T[]>(this._config.getCollectionUrl(collection));
  }

  /**
   *
   * @param collection
   * @param id
   * @returns single entity
   */
  public getSingle<T>(
    collection: REST_ENDPOINT_COLLECTIONS,
    id: number
  ): Observable<T> {
    return this._http.get<T>(
      `${this._config.getCollectionUrl(collection)}/${id}`
    );
  }

  /**
   *
   * @param collection
   * @param item
   * @returns server response
   */
  public update<T>(collection: REST_ENDPOINT_COLLECTIONS, item: T) {
    return this._http.put<T>(this._config.getCollectionUrl(collection), item);
  }

  /**
   *
   * @param collection
   * @param item
   * @returns server response
   */
  public create<T>(collection: REST_ENDPOINT_COLLECTIONS, item: T) {
    return this._http.post<T>(this._config.getCollectionUrl(collection), item);
  }
}
