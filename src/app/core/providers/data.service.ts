// Angular
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// Rxjs
import { Observable } from 'rxjs';

// third party
import { InMemoryDbService } from 'angular-in-memory-web-api';

import { Book } from '../../books/models/book.model';

@Injectable({
  providedIn: 'root',
})
export class DataService implements InMemoryDbService {
  public createDb(): {} {
    const books: Book[] = [
      new Book({
        id: 1,
        title: 'Test Book #1',
        author: 'John Foobar',
        genre: 'Murder Mystery',
        isbn: 'xxx-111-222-333-444-99',
        pages: 189,
        yearPublished: 2020,
      }),
      new Book({
        id: 2,
        title: 'Test Book #2',
        author: 'Testy McTestorson',
        genre: 'Cookbooks',
        isbn: 'yyyy-2222-666-88-66666-111',
        pages: 215,
        yearPublished: 1955,
      }),
    ];

    return { books };
  }
}
