// angular
import { Injectable } from '@angular/core';

import { REST_ENDPOINT_COLLECTIONS } from '../models/rest-endpoint-collections.enum';

@Injectable({
  providedIn: 'root',
})
export class RestConfigService {
  private readonly _localUrl: string = 'http://localhost:8080/api';
  private readonly _data: Map<REST_ENDPOINT_COLLECTIONS, string> = new Map();

  constructor() {
    this._data.set(REST_ENDPOINT_COLLECTIONS.BOOKS, 'books');
  }

  public getCollectionUrl(collection: REST_ENDPOINT_COLLECTIONS): string {
    const collectionName = this._data.get(collection) as string;
    return `${this._localUrl}/${collectionName}`;
  }
}
