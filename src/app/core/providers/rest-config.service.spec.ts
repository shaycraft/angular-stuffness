import { TestBed } from '@angular/core/testing';

import { RestConfigService } from './rest-config.service';

describe('RestConfigService', () => {
  let service: RestConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RestConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
